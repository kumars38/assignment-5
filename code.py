import math
import random
from xlsxwriter import Workbook
import timeit
from heap import Heap

#to change which build we are testing, we changed the build that is selected in the heap class 

def create_random_list(n):
    L = []
    for _ in range(n):
        L.append(random.randint(1,n))
    return L
def two_test():
    workbook = Workbook('sort4.xlsx')
    worksheet = workbook.add_worksheet()
    times =[]
    for i in range(100,10100,100):
        L = create_random_list(i)
        time_sum =0
        for j in range(100):
            start = timeit.default_timer()
            Heap(L)
            end = timeit.default_timer()
            time_sum += (end-start)
        times.append(time_sum/100)
    n = 1
    for i in range(100):
        worksheet.write(i, 0, 100*n)
        worksheet.write(i, 1, times[i])
        n += 1
    workbook.close() 

    
two_test()