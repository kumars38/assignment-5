import random

# Note that this K_Heap is 1-indexed instead of 0-indexed
class K_Heap:

    # Assumed that k > 1: at least a binary heap
    def __init__(self, values, k):
        # Add a value to the front for 1-indexing
        self.data = [0] + values 
        self.k = k
        self.length = len(values)
        self.build_heap(values)

    def build_heap(self, values):
        lastNonLeaf = self.parent(self.length)
        # Build the heap by sinking backwards from the last non-leaf node
        for i in range(lastNonLeaf, 0, -1):
            self.sink(i)

    # Returns 0 for the parent of the root node (data[1])
    def parent(self, i):
        return (i + self.k - 2) // self.k

    def children(self, i):
        startI = (i - 1) * self.k + 2
        endI = startI + self.k
        return [i for i in range(startI, endI)]

    def sink(self, i):
        largest_known = i
        children = self.children(i)
        # Find largest child value
        for child in children:
            if child <= self.length and self.data[child] > self.data[largest_known]:
                largest_known = child
        # Make sink swap
        if largest_known != i:
            self.data[i], self.data[largest_known] = self.data[largest_known], self.data[i]
            self.sink(largest_known)
